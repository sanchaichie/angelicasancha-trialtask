# TRIAL TASK

## Getting Started


### Install from repository

Download or clone repository, navigate to the directory and run:

    composer install

After it has completed, run:

    npm install

Copy the example .env file:

    cp .env.example .env

Generate an application key:

    php artisan key:generate

Migrate database:

    php artisan migrate

Run Mix tasks:

    npm run dev
 
