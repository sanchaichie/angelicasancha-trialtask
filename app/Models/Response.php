<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
	
    protected $table = 'responses';
    public $timestamps = true;
    protected $fillable = [
        'drawings_paye', 'been_in_business', 'years', 'job_type', 'staff_count', 'on_tools', 'shareholders_director_on_tools', 'income', 'payment_frequency','acc_cover_plan','cover_plus_extra_cover_amount'
    ];

}
