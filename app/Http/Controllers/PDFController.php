<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Models\Response;

class PDFController extends Controller
{
	
	function pdf($id)
	{
		$pdf = \App::make('dompdf.wrapper');
		$pdf->loadHTML($this->getLayout($id));
		return $pdf->stream();
	}

	function getLayout($id)
	{
		$response = Response::find($id);
		$output = '
		<table class="table" style="border:1px solid black;padding:8px;">
			<thead style="background-color:gray; color:white;">
				<tr>
					<th scope="col" style="padding:5px; text-align:center;">#</th>
					<th scope="col" style="padding:5px; text-align:center;">Question</th>
					<th scope="col" style="padding:5px; text-align:center;">Response</th>
				</tr>
			</thead>

			<tbody>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					1
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				Are you taking Drawings or Paye?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->drawings_paye
				.'
				</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					2
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				Been in business in more than 3 years?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->been_in_business
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					3
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				How many years?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->years
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					4
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				Working full time?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->job_type
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					5
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				How many staff do you have working for you?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->staff_count
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					6
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				Now, Are you on tools?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->on_tools
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					7
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				Is there any other shareholders/ director that are not on the tools?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->shareholders_director_on_tools
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					8
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				How much income did you take out last year from the business?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->income
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					9
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				Payment Frequency
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->payment_frequency
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					10
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				What ACC cover plan do you have?
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->acc_cover_plan
				.'</td>
			</tr>

			<tr>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
					11
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">
				Your nominated Cover Plus Extra cover amount
				</td>
				<td style="border-bottom:1px solid lightgray; padding:5px;">'.
				$response->cover_plus_extra_cover_amount
				.'</td>
				</tr>
			</tbody>
		</table>
		';  
		
		return $output;
	}
}
