<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('drawings_paye');
            $table->string('been_in_business');
            $table->string('years');
            $table->string('job_type');
            $table->string('staff_count');
            $table->string('on_tools');
            $table->string('shareholders_director_on_tools');
            $table->string('income');
            $table->string('payment_frequency');
            $table->string('acc_cover_plan');
            $table->string('cover_plus_extra_cover_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
