require('./bootstrap');


window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import ResponseList from './components/Form/ResponseList.vue';
import Create from './components/Form/Create.vue';
import Edit from './components/Form/Edit.vue';
import Show from './components/Form/Show.vue';

const routes = [
  {
      name: 'Response List',
      path: '/',
      component: ResponseList
  },
  {
      name: 'create',
      path: '/create-response',
      component: Create
  },

  {
      name: 'Edit',
      path: '/edit-response/:id',
      component: Edit
  },
  {
      name: 'Show',
      path: '/show-response/:id',
      component: Show
  }
];

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');